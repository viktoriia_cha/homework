// Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.

document.write("<p> Завдання 1");
document.write("<br>");

var array = [2, 5, 8, 15, 30];
document.write(array, "<br>");

function map(fn, array) {
    var newArray = [];
    for(var i = 0; i < array.length; i++) {
        newArray[i] = fn(array[i]);
    }
    return newArray

};

function fn (double) {
    return double * 2;
};
document.write(map(fn, array));


/* Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.
1	function checkAge(age) {
2	if (age > 18) {
3	return true;
4	} else {
5	return confirm('Родители разрешили?');
6	} }
*/
document.write("<p> Завдання 2");
document.write("<br>");

var age = prompt("Скільки тобі років?")
function checkAge(age) {
    return (age > 18) ? true : confirm('Батьки дозволили?')
}
document.write(checkAge(age));