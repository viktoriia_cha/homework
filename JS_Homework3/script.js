const styles = ["Джаз", "Блюз"];

const stylesAdded = styles.concat("Рок-н-ролл");
document.write("<p>Додано елемент: " + stylesAdded.join(", "));

document.write("<hr>");

const stylesMedium = stylesAdded.splice(stylesAdded[Math.floor(stylesAdded.length/2)] = "Классика");
document.write("<p>Замінено: " + stylesMedium.join(", "));

document.write("<hr>");

const stylesFirstDelete = stylesMedium.splice(0, 1); 
document.write("<p>Видалено: " + stylesFirstDelete.join(", "));
document.write("<p>Залишилось: " + stylesMedium.join(", "));

document.write("<hr>");

stylesMedium.unshift("Рэп", "Регги"); 
document.write("<p>Додано: " + stylesMedium.join(", "));
debugger

document.write("<p> Додаткові завдання");

const aT1 = ['a', 'b', 'c'];
const bT1 = [1, 2, 3]
const dT1 = aT1.concat(bT1);
document.write("<p> Завдання 1");
document.write("<p>" + dT1.join(", "));

document.write("<hr>")

const aT2 = ['a', 'b', 'c'];
const bT2 = aT2.push(1, 2, 3);
document.write("<p> Завдання 2");
document.write("<p>" + aT2.join(", "));

document.write("<hr>")

const aT3 = [1, 2, 3];
aT3.reverse();
document.write("<p> Завдання 3");
document.write("<p>" + aT3);

document.write("<hr>")

const aT4 = [1, 2, 3];
const bT4 = aT4.push(4, 5, 6);
document.write("<p> Завдання 4");
document.write("<p>" + aT4.join(", "));

document.write("<hr>")

const aT5 = [1, 2, 3];
aT5.unshift(4, 5, 6);
document.write("<p> Завдання 5");
document.write("<p>" + aT5.join(", "));

document.write("<hr>")

const aT6 = ['js', 'css', 'jq'];
document.write("<p> Завдання 6");
document.write("<p>" + aT6[0]);

document.write("<hr>")

const aT7 = [1, 2, 3, 4, 5];
const bT7 = aT7.slice(0, 3);
document.write("<p> Завдання 7");
document.write("<p>" + bT7)

document.write("<hr>")


const aT8 = [1, 2, 3, 4, 5];
const bT8 = aT8.splice(1, 2);
document.write("<p> Завдання 8");
document.write("<p>" + aT8)

document.write("<hr>")

const aT9 = [1, 2, 3, 4, 5];
const bT9 = aT9.splice(2, 0, 10);
document.write("<p> Завдання 9");
document.write("<p>" + aT9)

document.write("<hr>")

const aT10 = [3, 4, 1, 2, 7];
aT10.sort();
document.write("<p> Завдання 10");
document.write("<p>" + aT10.join(", "));

document.write("<hr/>");

const aT11 = ["Привіт, ", "світ", "!"];
document.write("<p> Завдання 11");
document.write("<p>" + aT11[0] + aT11[1] + aT11[2]);

document.write("<hr/>");

const aT12 = ["Привіт, ", "світ", "!"];
const bT12 = aT12.splice(0, 1, "Бувай, ")
document.write("<p> Завдання 12");
document.write("<p>" + aT12[0] + aT12[1] + aT12[2]);

