/*Створіть масив styles з елементами «Джаз» та «Блюз».
 Додайте "Рок-н-рол" в кінець.
Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною.
Видаліть перший елемент масиву та покажіть його.
Вставте «Реп» та «Реггі» на початок масиву.<br>
*/
document.write("<p> Завдання 1");
document.write("<br>");

const styles = ["Джаз", "Блюз"];

const stylesAdded = styles.concat("Рок-н-рол");
document.write("<p>Додано елемент: " + stylesAdded.join(", "));

const stylesMedium = stylesAdded.splice(stylesAdded[Math.floor(stylesAdded.length/2)] = "Класика");
document.write("<p>Замінено: " + stylesMedium.join(", "));

const stylesFirstDelete = stylesMedium.splice(0, 1); 
document.write("<p>Видалено: " + stylesFirstDelete.join(", "));
document.write("<p>Залишилось: " + stylesMedium.join(", "));

stylesMedium.unshift("Реп", "Реггі"); 
document.write("<p>Додано: " + stylesMedium.join(", "));
debugger

document.write("<hr>");

/* Напишіть функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив. */

document.write("<p> Завдання 2");
document.write("<br>");

var array = [2, 5, 8, 15, 30];
document.write(array, "<br>");

function map(fn, array) {
    var newArray = [];
    for(var i = 0; i < array.length; i++) {
        newArray[i] = fn(array[i]);
    }
    return newArray
};

function fn (double) {
    return double * 2;
};
document.write(map(fn, array));

document.write("<hr>");

/* Перепишіть функцію за допомогою оператора '?' або '||'
Наступна функція повертає true, якщо параметр age більше 18. В іншому випадку вона ставить запитання confirm і повертає його результат.
function checkAge(age) {
if (age > 18) {
return true;
} else {
return confirm('Батьки дозволили?');
} } */

document.write("<p> Завдання 3");
document.write("<br>");

var age = prompt("Скільки тобі років?")
function checkAge(age) {
    return (age > 18) ? true : confirm('Батьки дозволили?')
}
document.write(checkAge(age));